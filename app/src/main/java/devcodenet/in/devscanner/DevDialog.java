package devcodenet.in.devscanner;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;


public class DevDialog {

    public  static void showInternetDialog(final Context context,String barcode)
    {
        final Dialog dialog=new Dialog(context);
        dialog.setContentView(R.layout.devdialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView txtContent=dialog.findViewById(R.id.txtContent);
        txtContent.setText(barcode);
        dialog.findViewById(R.id.done)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       dialog.dismiss();
                       DevScanner dd=new DevScanner();
                       dd.takeBarcodePicture();
                    }
                });

    }
}
